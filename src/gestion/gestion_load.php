<?php
  require_once ("../bdd/bdd.php");
  // Premiere partie du formulaire pour l'insertion
  function displayFormInsert (){
      $bdd = connectBDD();
      $sql = "SELECT table_name FROM information_schema.tables WHERE information_schema.tables.table_schema LIKE 'home%' AND table_name NOT IN (SELECT table_name FROM information_schema.views WHERE information_schema.views.table_schema LIKE 'home%') AND table_name not like 'type' ORDER BY table_name";
      $req = $bdd -> prepare($sql);
      $req -> execute();
      echo "<h1> Insertion </h1> \n";
      if($req-> rowCount()){
        echo "<form action='src/gestion/gestion_form.php' method='post'> \n";

        echo "<label for='table'> Que désirez vous ajouter ? </label> ";
        echo "<select name='table'>";
        echo "<option selected='selected'> Faites votre choix </option>\n";
      while ($data= $req->fetch()){
        if($data['table_name'] != "emprunt")
          echo "<option value='".$data['table_name']."'>".$data['table_name']."</option> \n";
      }
      echo "</select>";
      echo "<input type='submit' type='submit' value='Submit' />";
      echo "</form>";
    }
  }


  function displayEmprunt(){
      $bdd = connectBDD();
      $sql="SELECT u.id as id, u.prenom as prenom, u.nom as nom, o.nom as nom_obj, e.ep_actif as actuel from users u, objets o, emprunt e where u.id = e.id_ut AND e.id_obj= o.id ";
      $req=$bdd->prepare($sql);
      $req->execute();

      echo "<h1> Emprunts de la Base</h1>";
      echo "<p>
      ici se trouve la liste de tous les éléments qui ont étés empruntés dans la base de donnée ainsi que tout ceux qui les ont empruntés
      </p> \n";

      echo "<table> \n";
      echo "<tr> \n";
      echo "<th>ID_USER</th>";
      echo "<th>Prenom</th>";
      echo "<th>Nom</th>";
      echo "<th> Nom Objet </th>";
      echo "<th>Emprunt Actif ? </th>";
      echo "</tr>";

      while ($data = $req->fetch()){
        echo "<tr> \n";
        echo "<td>".$data['id']."</td> \n";
        echo "<td>".$data['prenom']."</td> \n";
        echo "<td>".$data['nom']."</td> \n";
        echo "<td>".$data['nom_obj']."</td> \n";
        if($data['actuel'])
          echo "<td> OUI </td> \n";
        else
            echo "<td> NON </td> \n";
        echo "</tr> \n";
      }
        echo "</table> \n";
  }


  function displayFormInsertStep2($table){
    if($table== 'objets'){
      echo "<form action='src/gestion/gs_insert.php' method='post'> \n";

      echo "<input type='hidden' name='table' value='".$table."'/> \n";
      echo "  <label for='nom'> Nom: </label> \n ";
      echo "    <input type='text' name='nom' /> \n";

      echo "  <label for='unit'> Unite_Stockage: </label> \n ";
      echo "   <select name='unit'> \n";
      echo "      <option selected='selected'> Faites votre choix </option>\n";
      $bdd = connectBDD();
      $sql = "SELECT id, nom, piece, batiment FROM unite_stockage ORDER BY nom";
      $req = $bdd -> prepare($sql);
      $req -> execute();
      while ($data = $req->fetch()){
        echo "      <option value='".$data['id']."'>".$data['nom']."(".$data['piece'].", ".$data['batiment'].")</option> \n";
      }
      echo "    </select> \n";

      echo "  <label for='type'> Type de l'objet</label> \n";
      echo "    <select name='type'> \n";
      echo "      <option selected='selected'> Faites votre choix </option>\n";

      $bdd = connectBDD();
      $sql = "SELECT id, nom FROM type_objets";
      $req = $bdd -> prepare($sql);
      $req -> execute();
      while ($data = $req->fetch()){
        echo "      <option value='".$data['id']."'>".$data['nom']."</option> \n";
      }
      echo "   </select> \n";
      echo "  <input type='submit' type='submit' value='Submit' />\n";
      echo "</form>\n";
    }
    else if ($table == 'type_objets'){
      echo "<form action='src/gestion/gs_insert.php' method='post'> \n";

      echo "<input type='hidden' name='table' value='".$table."'/> \n";
      echo "  <label for='nom'> Nom: </label> \n ";
      echo "    <input type='text' name='nom' /> \n";
      echo "  <input type='submit' type='submit' value='Submit' />\n";

      echo "</form>\n";
    }
    else if ($table == 'unite_stockage'){
      echo "<form action='src/gestion/gs_insert.php' method='post'> \n";

      echo "<input type='hidden' name='table' value='".$table."'/> \n";
      echo "  <label for='nom'> Nom: </label> \n ";
      echo "    <input type='text' name='nom' /> \n";
      echo "  <label for='piece'> Piece: </label> \n ";
      echo "   <input type='text' name='piece' /> \n";
      echo "  <label for='batiment'> Batiment: </label> \n ";
      echo "   <input type='text' name='batiment' /> \n";

      echo "  <input type='submit' type='submit' value='Submit' />\n";

      echo "</form>\n";
    }
    else if ($table == 'users'){
      echo "<form action='src/gestion/gs_insert.php' method='post'> \n";

      echo "<input type='hidden' name='table' value='".$table."'/> \n";
      echo "  <label for='nom'> Prénom: </label> \n ";
      echo "    <input type='text' name='nom' /> \n";
      echo "  <label for='prenom'> Nom: </label> \n ";
      echo "   <input type='text' name='prenom' /> \n";
      echo "  <label for='mdp'> Mot de passe : </label> \n ";
      echo "   <input type='password' name='mdp' /> \n";
      echo "  <label for='mdp'> Confirmation mot de passe : </label> \n ";
      echo "   <input type='password' name='cmdp' /> \n";

      echo "  <input type='submit' type='submit' value='Submit' />\n";

      echo "</form>\n";
    }
  }

  function displayErreur($errNo){
    switch($errNo){
      case 1:
        echo "<p class='erreur'> Erreur 01 : un des champs n'est pas rempli </p>";
        break;
      case 2:
        echo "<p class='erreur'> Erreur 02 : faites un choix pour les listes </p>";
        break;
      case 3:
        echo "<p class='erreur'> Erreur 03, dans l'insertion de votre valeur, réessayez s'il vous plaît ! </p>";
        break;
      case 4:
        echo "<p class='erreur'> Erreur 04 : Element dejà présent dans la base </p>";
        break;
      case 5:
        echo "<p class='erreur'> Erreur 05 : les mots de passes ne correspondent pas </p>";
    }
  }
?>
