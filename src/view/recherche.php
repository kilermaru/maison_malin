<html>
	<head>
		<meta charset="utf-8" />
		<title>Recherche</title>
			<?php include("header.php"); ?>
	</head>
  <body>
    <?php include ("nav.php"); ?>
		<?php
		if(isset($_SESSION['identifiant'])){
			include("../research/rs_form.php");
			if ($_SESSION['identifiant'] == 2160001) {
				include("gestion_nav.php");
			}
			if (!isset($_GET['type'])) {
					researchTable();
			}
			else {
				if(isset($_GET['val']) AND isset($_GET['col'])) {
					formSort($_GET['type'], $_GET['col'], $_GET['val']);
					if(isset($_GET['sort'])){
						displayResultResearch($_GET['type'], $_GET['col'], $_GET['val'],$_GET['sort'], $_SESSION['identifiant']);
					}
					else {
						displayResultResearch($_GET['type'], $_GET['col'], $_GET['val'],'nom', $_SESSION['identifiant']);
					}
				}
				else{
				researchColumn($_GET['type']);
			}
		}
	}
	else echo "<h2> Erreur : vous n'etes pas connectés </h2>";
?>

  </body>
</html>
