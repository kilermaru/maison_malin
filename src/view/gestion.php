<html>
	<head>
		<meta charset="utf-8" />
		<title>Gestion</title>
			<?php include("header.php"); ?>
	</head>
  <body>
    <?php include ("nav.php"); ?>
		<?php
			if(isset($_SESSION['identifiant']) && $_SESSION['identifiant'] == 2160001){
				include("../gestion/gestion_load.php");
				include("gestion_nav.php");

				if(isset($_GET['insert'])){
					if(isset($_GET['table'])){
						if(isset($_GET['erreur'])){
							displayErreur($_GET['erreur']);
						}
						displayFormInsertStep2($_GET['table']);
					}
					else {
						displayFormInsert();
					}
				}
				if(isset($_GET['emprunt'])){
				  displayEmprunt();
				}
			}
			else echo "<h1> Veuillez Vous connecter pour accèder à cette page </h1>";
		?>
  </body>
</html>
