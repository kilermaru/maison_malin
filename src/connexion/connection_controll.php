<?php
  require('function_member.php');

  if (empty($_POST['identifiant']) || empty($_POST['password']))
  {
    header ("Location: ../../connection?erreur=1");
    exit();
  }
  $identifiant = $_POST['identifiant'];
  $mot_de_passe = $_POST['password'];
  $champs = "MDP";
  $mot_de_passe_bdd = selectCustomMember($champs, $identifiant)['MDP'];
  if ($mot_de_passe == $mot_de_passe_bdd)
  {
    session_start();
    $_SESSION["identifiant"]=$identifiant;
    header ("Location: ../../accueil");
  }
  else {
    header ("Location: ../../connection?erreur=2");
  }
?>
