<?php

  require_once('../bdd/bdd.php');

  function getMember($identifiant)
  {
    //Retourne les informations concernant $identifiant en tant qu'un tableau indexé par le nom des colonnes
    $bdd = connectBDD();
    $sql = "SELECT * FROM users WHERE IDENTIFIANT='".$identifiant."'";
    $req = $bdd -> prepare($sql);
    $req -> execute();
    $result = $req -> fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  function selectCustomMember($champs, $identifiant)
  {
    //Permet de faire un SELECT sur le table users avec une condition sur l'identifiant en personnalisant les champs voulus.
    //$champs viendra se glisser entre le SELECT et le FROM
    $bdd = connectBDD();
    $sql = "SELECT ";
    $sql .= $champs;
    $sql .= " FROM users WHERE ID='".$identifiant."'";
    echo("<p>".$sql."</p>");
    $req = $bdd -> prepare($sql);
    $req -> execute();
    $result = $req -> fetch(PDO::FETCH_ASSOC);
    close($req);
    return $result;
  }

  function addMember($data)
  {
    //ajoute un users dont les informations sont donné par un tableau indexé par le nom des colonnes en miniscules (on prendra souvent $_POST)
    $bdd = connectBDD();
    $sql = "INSERT INTO users (ID,PRENOM, NOM, MDP ) VALUES (:id, :prenom, :nom, :mdp )";
    $req = $bdd -> prepare($sql);
    $req -> execute(array(
      "id" => $data['id'],
      "mdp" => $data['mdp'],
      "prenom" => $data['prenom'],
      "nom" => $data['nom']
    ));
    return $req;
  }
 ?>
