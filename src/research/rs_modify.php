<?php
    require_once('../bdd/bdd.php');
    $bdd = connectBDD();

    $id = $_POST['id'];

    if(isset($_POST['modif'])){
      //initialisation des éléments du formulaire

      $nom = $_POST['nom'];
      $type = $_POST['type'];
      $stockage = $_POST['stockage'];
      $piece = $_POST['piece'];
      $batiment = $_POST['batiment'];

      //selection de l'unité de stockage
      $sql = "SELECT id,nom, piece, batiment FROM unite_stockage WHERE nom like '%".$stockage."%' AND piece like '%".$piece."%' AND batiment like '%".$batiment."%'";
      $reqStock = $bdd -> prepare($sql);
      $reqStock -> execute();

      //si cette unité de stockage n'est pas encore dans la base, on l'insère directement, puis on en récupère l'id.
      if (!$reqStock->rowCount()){
        $sql = "INSERT INTO unite_stockage (`id`, `nom`, `piece`, `batiment`) VALUES (NULL, '".$stockage."', '".$piece."', '".$batiment."')";
        $reqStoc = $bdd -> prepare($sql);
        $reqStoc -> execute();
        $sql = "SELECT id FROM unite_stockage WHERE nom like '%".$stockage."%' AND piece like '%".$piece."%' AND batiment like '%".$batiment."%'";
        $reqStock = $bdd -> prepare($sql);
        $reqStock -> execute();

      }

      //on récupère le type de l'objet
      $IdStock = $reqStock->fetch();
      $sql = "SELECT id FROM type_objets WHERE nom like '%".$type."%'";
      $reqType = $bdd -> prepare($sql);
      $reqType -> execute();

      //si il n'est pas inséré on le créé
      if (!$reqType->rowCount()){
        $sql = "INSERT INTO type_objets (`id`, `nom`) VALUES (NULL, '".$type."')";
        $reqType = $bdd -> prepare($sql);
        $reqType -> execute();
        $sql = "SELECT id FROM type_objets WHERE nom like '%".$type."%'";
        $reqType = $bdd -> prepare($sql);
        $reqType -> execute();
      }

      //ensuite on conserve son id
      $IdType = $reqType->fetch();

      //on regarde l'objet modifié dans la base
      $sql = "SELECT id,nom, id_stockage FROM objets WHERE id = ".$id;
      $reqObj = $bdd -> prepare($sql);
      $reqObj -> execute();
      if(!$reqObj->rowCount()){
        $sql = "INSERT INTO objets (`id`,`nom`, `id_stockage`) VALUES (NULL, '".$nom."', '".$IdStock['id']."')";
        $reqObj = $bdd -> prepare($sql);
        $reqObj -> execute();
      }
      else {
        $sql = "UPDATE objets SET nom='".$nom."', id_stockage = '".$IdStock['id']."' WHERE id =".$id;
        $reqObj = $bdd -> prepare($sql);
        $reqObj -> execute();
      }

      $sql = "DELETE FROM type where id_obj = ".$id;
      $reqTypeDel = $bdd -> prepare($sql);
      $reqTypeDel -> execute();

      $sql = "INSERT INTO type (id_obj, id_type) VALUES ('".$id."', '".$IdType['id']."')";
      $reqTypeDel = $bdd -> prepare($sql);
      $reqTypeDel -> execute();

      header("Location: ../../recherche?type=".$_POST['table']."&col=".$_POST['column']."&val=".$_POST['texte']);
      exit();
    }
    else if(isset($_POST['emprunt'])){
            if($_POST['emprunt'] == 'Emprunter'){
                $sql= "INSERT INTO emprunt(id_obj, id_ut, ep_actif) VALUES ('".$id."', '".$_POST['user_id']."', 1)";

              }
              else if($_POST['emprunt'] == 'Fin Emprunt'){
                $sql="UPDATE emprunt SET ep_actif=0 where id_ut=".$_POST['user_id']." and ep_actif=1 and id_obj=".$id;
              }
            $req= $bdd->prepare($sql);
            $req->execute();
            header("Location: ../../recherche?type=".$_POST['table']."&col=".$_POST['column']."&val=".$_POST['texte']);
            exit();
    }

 ?>
