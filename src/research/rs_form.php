<?php
  require_once('../bdd/bdd.php');
  function researchTable(){
    $bdd = connectBDD();
    $sql = "SELECT table_name FROM information_schema.views WHERE information_schema.views.table_schema LIKE 'home%'";
    $req = $bdd -> prepare($sql);
    $req -> execute();
    echo "<h1> Recherche </h1> \n";
    if($req-> rowCount()){
      echo "<form action='src/research/rs_first_step.php' method='post'> \n";
      echo "<label for='table'> Que désirez vous rechercher ? </label> ";
      echo "<select name='table'>";
      echo "<option selected='selected'> Faites votre choix </option>\n";
    while ($data= $req->fetch()){
      echo "<option value='".$data['table_name']."'>".$data['table_name']."</option> \n";
    }
    echo "</select>";
    echo "<input type='submit' type='submit' value='Submit' />";
    echo "</form>";
  }
  else echo ("Aucune table dans la base de donnée home, base de donnée cassée !");
}
  function researchColumn($type){
      $bdd = connectBDD();
      $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '".$type."' AND TABLE_SCHEMA = 'home' AND COLUMN_NAME NOT LIKE '%id%'";
      $req = $bdd -> prepare($sql);
      $req -> execute();


      echo "<h1> Recherche </h1> \n";
      if($req-> rowCount()){
      echo "<form action='src/research/rs_second_step.php' method='post'> \n";
      echo "<input type='hidden' name='table' value='".$type."'/> \n";
      echo "<label for='column'> Chercher par : </label> \n";
      echo "<select name='column'> \n";
      echo "<option selected='selected'> Faites votre choix </option>\n";
      while ($data= $req->fetch()){
        echo "<option value='".$data['COLUMN_NAME']."'>".$data['COLUMN_NAME']."</option> \n";
      }
      echo "</select> \n";
      echo "<label for='texte'> Texte de la recherche : </label> \n ";
      echo "<input type='text' name='texte' /> \n";
      echo "<input type='submit' type='submit' value='Submit' /> \n";
      echo "</form> \n";
      }
  }
  function formSort($type, $col, $val){
    $bdd = connectBDD();
    $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '".$type."' AND TABLE_SCHEMA = 'home' AND COLUMN_NAME NOT LIKE '%id%'";
    $req = $bdd -> prepare($sql);
    $req -> execute();


    echo "<h1> Recherche :</h1> \n";
    if($req-> rowCount()){
    echo "<form action='src/research/rs_sort_treatement.php' method='post'> \n";
    echo "<input type='hidden' name='table' value='".$type."'/> \n";
    echo "<input type='hidden' name='column' value='".$col."'/> \n";
    echo "<input type='hidden' name='texte' value='".$val."'/> \n";
    echo "<label for='column'> Trier Par : </label> \n";
    echo "<select name='sort'> \n";
    echo "<option selected='selected'> Faites votre choix </option>\n";
    while ($data= $req->fetch()){
      echo "<option value='".$data['COLUMN_NAME']."'>".$data['COLUMN_NAME']."</option> \n";
    }
    echo "</select> \n";
    echo "<input type='submit' type='submit' value='Submit' /> \n";
    echo "</form> \n";
  }
  }







  function displayResultResearch($type, $col, $val, $sort, $user_id){

    $bdd = connectBDD();
    $sql = "SELECT c.COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS c WHERE TABLE_NAME = '".$type."' AND TABLE_SCHEMA = 'home'";
    $requete = $bdd -> prepare($sql);
    $requete -> execute();
    $i =1;
    echo "<h2> Resultats de type : ".$type.". Pour : ".$col."=".$val." : </h2> \n";
    echo "<table> \n";
    echo "<caption> Resultats de la recherche : </caption> \n";
    echo "<tr> \n";
    echo "<th class='numero'> numéro </th> \n";
    if($requete-> rowCount()){
      while ($data= $requete->fetch()){
        if($data['COLUMN_NAME'] != "id"){
            echo "<th><a href='recherche?type=".$type."&col=".$col."&val=".$val."&sort=".$data['COLUMN_NAME']."'>".$data['COLUMN_NAME']."</a></th> \n";
        }
      }
    }
    if($type=='total_infos_objets'){
      echo "<th> Modifier </th> \n";
      echo "<th> Emprunter </th> \n";
    }

    echo"</tr> \n";

    $sql = "SELECT * FROM ".$type;
    if($val != ""){
      $sql = $sql." WHERE ".$col." LIKE '%".$val."%'";
    }

    $sql = $sql." ORDER BY ".$sort;
    $requete = $bdd -> prepare($sql);
    $requete -> execute();
    if ($type == "total_infos_objets"){
      while ($data = $requete->fetch()){
        echo "<tr> \n";
        echo "<form action='src/research/rs_modify.php' method='post'> \n";
        echo "<input type='hidden' name='table' value='".$type."'/> \n";
        echo "<input type='hidden' name='column' value='".$col."'/> \n";
        echo "<input type='hidden' name='texte' value='".$val."'/> \n";
        echo "<input type='hidden' name='id' value='".$data['id']."'/> \n";
        echo "<input type='hidden' name='user_id' value='".$user_id."'/>\n";

        echo "<td>".$i."</td> \n";
        echo "<td><input type='text' name='nom' value ='".$data['nom']."'/></td> \n";
        echo "<td><input type='text' name='type' value ='".$data['type']."'/></td> \n";
        echo "<td><input type='text' name='stockage' value ='".$data['stockage']."'/></td> \n";
        echo "<td><input type='text' name='piece' value ='".$data['piece']."'/></td> \n";
        echo "<td><input type='text' name='batiment' value ='".$data['batiment']."'/></td> \n";
        echo "<td><input type='submit' name='modif' value='Modifier' /></td> \n";

        $sql = "SELECT id_obj, ep_actif FROM emprunt where ep_actif= 1 AND id_obj=".$data['id'];
        $req= $bdd->prepare($sql);
        $req->execute();

        if(!isset($req->fetch()['id_obj'])){
          echo "<td><input type='submit' name='emprunt' value='Emprunter' /></td> \n";
        }
        else{
          echo "<td><input type='submit' name='emprunt' value='Fin Emprunt' /></td> \n";
        }
        echo "</form> \n";
        echo"</tr> \n";
        $i++;
      }
    }
    else if ($type == 'batiment'){
      while ($data = $requete->fetch()){
        echo "<tr> \n";
        echo "<td>".$i."</td> \n";
        echo "<td>".$data['nom']."</td> \n";
        echo"</tr> \n";
        $i++;
      }
    }
    else if ($type == 'piece'){
      while ($data = $requete->fetch()){
        echo "<tr> \n";
        echo "<td>".$i."</td> \n";
        echo "<td>".$data['nom']."</td> \n";
        echo "<td>".$data['batiment']."</td> \n";
        echo"</tr> \n";
        $i++;
      }
    }

  }


?>
