<?php
function connectBDD()
{
  $bdd = NULL;
  try
  {
    $bdd = new PDO('mysql:host=localhost;dbname=home;charset=utf8', 'root', 'root');
  }
  catch(Exception $e)
  {
    die('Erreur : '.$e->getMessage());
  }
  return $bdd;
}

function close(&$pdo)
{
  $pdo -> closeCursor();
  $pdo = NULL;
}
?>
