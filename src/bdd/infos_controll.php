<?php
  require_once('bdd.php');


  function displayBat(){
    $bdd = connectBDD();
    $sql = "SELECT * FROM batiment ORDER BY nom";
    $req = $bdd -> prepare($sql);
    $req -> execute();
    $i = 1;

    echo"<h2> Batîments :</h2> \n";
    echo"<p> il y a : ".$req->rowcount()." batîments dans la base de données</p>";
    echo "<table> \n";
    echo "<caption> Liste des Batiments sur le terrain </caption> \n";
    echo "<tr> \n";
    echo "<th class='numero'> numéro </th> \n";
    echo "<th> nom </th> \n";
    echo "</tr> \n";
    if($req-> rowCount()){
      while ($data= $req->fetch()){
            echo "<tr> \n";
            echo "<td class='numero'> ".$i."</td>\n";
            echo "<td> ".$data['nom']."</td>\n";
            $i++;
            echo"</tr> \n";
      }
    }
    else {
      echo "<tr> \n";
      echo "<td> ".$i."</td>\n";
      echo "<td> Aucune piece </td>\n";
      echo"</tr> \n";
    }

  echo "</table>";
  }

  function displayPiece(){
    $bdd = connectBDD();
    $sql = "SELECT nom, batiment FROM piece order by nom";
    $req = $bdd -> prepare($sql);
    $req -> execute();
    $i = 1;
    echo"<h2> Pièces :</h2> \n";
    echo"<p> il y a : ".$req->rowcount()." pièces dans la base de données</p>";
    echo "<table> \n";
    echo "<caption> Liste des pièces de la maison </caption> \n";
    echo "<tr> \n";
    echo "<th class='numero'> numéro </th> \n";
    echo "<th> nom_Piece </th> \n";
    echo "<th> nom_Bat </th> \n";
    echo "</tr> \n";
    if($req-> rowCount()){
      while ($data= $req->fetch()){
            echo "<tr> \n";
            echo "<td class='numero'> ".$i."</td>\n";
            echo "<td> ".$data['nom']."</td>\n";
            echo "<td> ".$data['batiment']."</td>\n";
            $i++;
            echo"</tr> \n";
      }
    }
    else {
      echo "<tr> \n";
      echo "<td> ".$i."</td>\n";
      echo "<td> Aucune piece </td>\n";
      echo"</tr> \n";
    }

    echo "</table>";
  }

  function displayBigObj($page){
    $bdd = connectBDD();
    $sql = "SELECT * FROM total_infos_objets order by nom";
    $req = $bdd -> prepare($sql);
    $req -> execute();
    $i = 1;
    echo"<h2> Objets :</h2> \n";
    echo"<p> il y a : ".$req->rowcount()." objets dans la base de données</p>";
    echo "<table> \n";
    echo "<caption> Liste des objets de la propriété </caption> \n";
    echo "<tr> \n";
    echo "<th class='numero'> numéro </th> \n";
    echo "<th> nom_Objet </th> \n";
    echo "<th> type </th> \n";
    echo "<th> nom_Piece </th> \n";
    echo "<th> supprimer </th> \n";
    echo "</tr> \n";
    if($req-> rowCount()){
      while ($data= $req->fetch()){
            echo "<tr> \n";
            echo "<form action='src/bdd/info_traitement.php' method='post'> \n";
            echo "<input type='hidden' name='id' value='".$data['id']."'/> \n";
            echo "<input type='hidden' name='table' value='objets' /> \n";
            echo "<td class='numero'> ".$i."</td>\n";
            echo "<td> ".$data['nom']."</td>\n";
            echo "<td> ".$data['type']."</td>\n";
            echo "<td> ".$data['piece']."</td>\n";
            $i++;
            echo "<td><input type='submit' value='Supprimer' /></td> \n";
            echo "</form> \n";
            echo"</tr> \n";
      }
    }
    else {
      echo "<tr> \n";
      echo "<td> ".$i."</td>\n";
      echo "<td> Aucune piece </td>\n";
      echo"</tr> \n";
    }

    echo "</table>";
  }

  function displayUnitStock(){
    $bdd= connectBDD();
    $sql= "SELECT * from unite_stockage ORDER BY nom";
    $req = $bdd -> prepare($sql);
    $req -> execute();
    $i = 1;

    echo"<h2> Unité de stockage :</h2> \n";
    echo"<p> il y a : ".$req->rowcount()." unité dans la base de données</p>";
    echo "<table> \n";
    echo "<caption> Liste des unité de stockage de la propriété </caption> \n";
    echo "<tr> \n";
    echo "<th class='numero'> numéro </th> \n";
    echo "<th> nom_Unite </th> \n";
    echo "<th> piece </th> \n";
    echo "<th> batiment </th> \n";
    echo "<th> supprimer </th> \n";
    echo "</tr> \n";

    if($req-> rowCount()){
      while ($data= $req->fetch()){

            echo "<tr> \n";

            echo "<form action='src/bdd/info_traitement.php' method='post'> \n";
            echo "<input type='hidden' name='id' value='".$data['id']."'/> \n";
            echo "<input type='hidden' name='table' value='unite_stockage'/> \n";
            echo "<td class='numero'> ".$i."</td>\n";
            echo "<td> ".$data['nom']."</td> \n";
            echo "<td> ".$data['piece']."</td> \n";
            echo "<td> ".$data['batiment']."</td> \n";
            echo "<td><input type='submit' value='Supprimer' /></td> \n";
            echo "</form>\n";


            $i++;
            echo"</tr> \n";
      }
    }
    else {
      echo "<tr> \n";
      echo "<td> ".$i."</td>\n";
      echo "<td> Aucune unite </td>\n";
      echo"</tr> \n";
    }

    echo "</table>\n";
  }

  function displayUsers(){
    $bdd = connectBDD();
    $sql = "SELECT * FROM users order by id";
    $req = $bdd -> prepare($sql);
    $req -> execute();
    echo"<h2> Users :</h2> \n";
    echo"<p> il y a : ".$req->rowcount()." utilisateurs dans la base de données</p>";
    echo "<table> \n";
    echo "<caption> Liste des utilisateurs de la propriété </caption> \n";
    echo "<tr> \n";
    echo "<th> identifiant </th> \n";
    echo "<th> prénom </th> \n";
    echo "<th> nom </th> \n";
    echo "<th> mdp</th> \n";
    echo "<th> supprimer </th> \n";
    echo "</tr> \n";
    if($req-> rowCount()){
      while ($data= $req->fetch()){
            echo "<tr> \n";
            echo "<form action='src/bdd/info_traitement.php' method='post'> \n";
            echo "<input type='hidden' name='id' value='".$data['id']."'/> \n";
            echo "<input type='hidden' name='table' value='users' /> \n";
            echo "<td class='numero'> ".$data['id']."</td>\n";
            echo "<td> ".$data['prenom']."</td>\n";
            echo "<td> ".$data['nom']."</td>\n";
            echo "<td> ".$data['mdp']."</td>\n";
            echo "<td><input type='submit' value='Supprimer' /></td> \n";
            echo "</form> \n";
            echo"</tr> \n";
      }
    }
    else {
      echo "<tr> \n";
      echo "<td> ".$i."</td>\n";
      echo "<td> Aucune piece </td>\n";
      echo"</tr> \n";
    }

    echo "</table>";
  }

  function displayType(){
    $bdd = connectBDD();
    $sql = "SELECT * FROM type_objets order by nom";
    $req = $bdd -> prepare($sql);
    $req -> execute();
    $i = 1;
    echo"<h2> Types:</h2> \n";
    echo"<p> il y a : ".$req->rowcount()." types dans la base de données</p>";
    echo "<table> \n";
    echo "<caption> Liste des types de la base de données </caption> \n";
    echo "<tr> \n";
    echo "<th class='numero'> numéro </th> \n";
    echo "<th> nom </th> \n";
    echo "<th> supprimer </th> \n";
    echo "</tr> \n";
    if($req-> rowCount()){
      while ($data= $req->fetch()){
            echo "<tr> \n";
            echo "<form action='src/bdd/info_traitement.php' method='post'> \n";
            echo "<input type='hidden' name='id' value='".$data['id']."'/> \n";
            echo "<input type='hidden' name='table' value='type_objets' /> \n";
            echo "<td class='numero'> ".$i."</td>\n";
            echo "<td> ".$data['nom']."</td>\n";
            $i++;
            echo "<td><input type='submit' value='Supprimer' /></td> \n";
            echo "</form> \n";
            echo"</tr> \n";
      }
    }
    else {
      echo "<tr> \n";
      echo "<td> ".$i."</td>\n";
      echo "<td> Aucune piece </td>\n";
      echo"</tr> \n";
    }

    echo "</table>";

  }
?>
