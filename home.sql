-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 13 Février 2017 à 15:33
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `home`
--

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `batiment`
--
CREATE TABLE `batiment` (
`nom` varchar(25)
);

-- --------------------------------------------------------

--
-- Structure de la table `emprunt`
--

CREATE TABLE `emprunt` (
  `id_ut` int(9) NOT NULL,
  `id_obj` int(6) NOT NULL,
  `ep_actif` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `emprunt`
--

INSERT INTO `emprunt` (`id_ut`, `id_obj`, `ep_actif`) VALUES
(2160001, 1, 0),
(2160001, 11, 0),
(2160001, 2, 0),
(2160001, 10, 0),
(2160001, 12, 0),
(2160001, 1, 0),
(2160001, 11, 0),
(2160001, 11, 0);

-- --------------------------------------------------------

--
-- Structure de la table `objets`
--

CREATE TABLE `objets` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `id_stockage` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `objets`
--

INSERT INTO `objets` (`id`, `nom`, `id_stockage`) VALUES
(1, 'Citroën C4 picasso bleue', 1),
(2, 'Lucky Luke Tome 1', 5),
(10, 'Lucky Luke Tome 2', 2),
(11, 'Lucky Lucke Tome 3', 5),
(12, 'Renault Scenic', 1);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `piece`
--
CREATE TABLE `piece` (
`nom` varchar(25)
,`batiment` varchar(25)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `total_infos_objets`
--
CREATE TABLE `total_infos_objets` (
`nom` varchar(30)
,`type` varchar(30)
,`stockage` varchar(25)
,`piece` varchar(25)
,`batiment` varchar(25)
,`id` int(11)
);

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

CREATE TABLE `type` (
  `id_obj` int(8) NOT NULL,
  `id_type` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `type`
--

INSERT INTO `type` (`id_obj`, `id_type`) VALUES
(2, 9),
(10, 13),
(11, 13),
(12, 1),
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `type_objets`
--

CREATE TABLE `type_objets` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `type_objets`
--

INSERT INTO `type_objets` (`id`, `nom`) VALUES
(1, 'véhicule'),
(3, 'vase'),
(4, 'bouteille'),
(5, 'remorque'),
(6, 'outils'),
(7, 'horloge'),
(8, 'jeu de société'),
(9, 'livre'),
(10, 'vin rouge'),
(11, 'livre'),
(13, 'bande dessinée'),
(15, 'cognac'),
(17, 'télévision'),
(18, 'tablette'),
(19, 'ordinateur'),
(22, 'test2');

-- --------------------------------------------------------

--
-- Structure de la table `unite_stockage`
--

CREATE TABLE `unite_stockage` (
  `id` int(11) NOT NULL,
  `nom` varchar(25) NOT NULL,
  `piece` varchar(25) NOT NULL,
  `batiment` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `unite_stockage`
--

INSERT INTO `unite_stockage` (`id`, `nom`, `piece`, `batiment`) VALUES
(1, 'emplacement voiture', 'garage', 'grange'),
(2, 'bibilothèque', 'salon', 'maison'),
(3, 'frigo', 'cuisine', 'maison'),
(4, 'frigo', 'buanderie', 'maison'),
(5, 'bibilothèque', 'home cinéma', 'maison'),
(6, 'test', 'test', 'test'),
(7, 'buffet', 'buanderie', 'maison');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(7) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `mdp` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `mdp`) VALUES
(2160001, 'Admin', 'Admin', 'admin');

-- --------------------------------------------------------

--
-- Structure de la vue `batiment`
--
DROP TABLE IF EXISTS `batiment`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `batiment`  AS  select distinct `unite_stockage`.`batiment` AS `nom` from `unite_stockage` ;

-- --------------------------------------------------------

--
-- Structure de la vue `piece`
--
DROP TABLE IF EXISTS `piece`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `piece`  AS  select distinct `unite_stockage`.`piece` AS `nom`,`unite_stockage`.`batiment` AS `batiment` from `unite_stockage` ;

-- --------------------------------------------------------

--
-- Structure de la vue `total_infos_objets`
--
DROP TABLE IF EXISTS `total_infos_objets`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `total_infos_objets`  AS  select `o`.`nom` AS `nom`,`ty`.`nom` AS `type`,`u`.`nom` AS `stockage`,`u`.`piece` AS `piece`,`u`.`batiment` AS `batiment`,`o`.`id` AS `id` from (((`objets` `o` join `type` `t`) join `unite_stockage` `u`) join `type_objets` `ty`) where ((`o`.`id` = `t`.`id_obj`) and (`t`.`id_type` = `ty`.`id`) and (`o`.`id_stockage` = `u`.`id`)) ;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `objets`
--
ALTER TABLE `objets`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `type_objets`
--
ALTER TABLE `type_objets`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `unite_stockage`
--
ALTER TABLE `unite_stockage`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `objets`
--
ALTER TABLE `objets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `type_objets`
--
ALTER TABLE `type_objets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT pour la table `unite_stockage`
--
ALTER TABLE `unite_stockage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2160003;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
